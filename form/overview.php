<?php
require('../dbconfig');

  $mail = "mcielen@gmail.com";
  $to = $mail;
  $subject = "Quizoverzicht";
  $headers = "From: root@ns1.cielen.org\r\n";
  $headers .= "Reply-To: root@ns1.cielen.org\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

  $message = '<html><body>';

  $message .= "<h2>Quizoverzicht</h2>";
  $message .= "<br>";

  // query om events te krijgen waar gebruiker niet op gereageerd heeft.
  $stmt = $pdo->query("select events.id as eventid, events.datetime, events.title, events.participants, events.address, reactions.id as reactionid, reactions.user_id, reactions.reaction from events LEFT JOIN reactions ON events.id = reactions.event_id GROUP BY events.id");

  $message .= "<h3>Geplande quizzen</h3>";
  while ($row = $stmt->fetch()){
    $eventid = $row['eventid'];
    $stmt_countpart = $pdo->query("select count(*) from reactions where reaction = 1 and event_id = $eventid");
    // query voor lijst deelnemers
    $stmt_partnames = $pdo->query("select name from events left join reactions on reactions.event_id = events.id join users on users.id = reactions.user_id where events.id = $eventid and reactions.reaction = 1");
    $stmt_partnonames = $pdo->query("select name from events left join reactions on reactions.event_id = events.id join users on users.id = reactions.user_id where events.id = $eventid and reactions.reaction = 0");
    $count = $stmt_countpart->fetchColumn();
    $message .= $row['datetime'] . "    " . $row['title'] . "    " . $count . "/" . $row['participants'] . " deelnemers    " . $row['address'];
    $message .= "<br>Doen mee: ";
    while ($rowp = $stmt_partnames->fetch()){
      $message .= $rowp['name'] . " ";
    }
    $message .= "<br>Doen niet mee: ";
    while ($rownp = $stmt_partnonames->fetch()){
      $message .= $rownp['name'] . " ";
    }
    $message .= "<br><br>";
  }

  $message .= "</table>";
  $message .= "</body></html>";
  //mail($to, $subject, $message, $headers);
  echo $message;
?>
