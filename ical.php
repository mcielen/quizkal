<?php
header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: inline; filename=calendar.ics');
require('dbconfig');
$user=$_SERVER['QUERY_STRING'];
$stmt = $pdo->query("SELECT events.* FROM events JOIN reactions ON reactions.event_id = events.id where reactions.user_id=$user and reactions.reaction=1;");
 
$output = "BEGIN:VCALENDAR\r
METHOD:PUBLISH\r
VERSION:2.0\r
PRODID:-//Happy Ending//Quizkalender//EN\r
BEGIN:VTIMEZONE\r
TZID:Europe/Brussels\r
X-LIC-LOCATION:Europe/Brussels\r
BEGIN:DAYLIGHT\r
TZOFFSETFROM:+0100\r
TZOFFSETTO:+0200\r
TZNAME:CEST\r
DTSTART:19700329T020000\r
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3\r
END:DAYLIGHT\r
BEGIN:STANDARD\r
TZOFFSETFROM:+0200\r
TZOFFSETTO:+0100\r
TZNAME:CET\r
DTSTART:19701025T030000\r
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10\r
END:STANDARD\r
END:VTIMEZONE\r\n";
 
while ($event = $stmt->fetch()){
$starts  = date('Ymd\THis', strtotime($event['datetime']));
$ends  = date('Ymd\T235959', strtotime($event['datetime']));
$output .=
"BEGIN:VEVENT\r
DTSTAMP:19971210T080000Z\r
ORGANIZER:HappyEnding\r
SUMMARY:" . $event['title'] . "\r
UID:" . $event['id'] . "\r
STATUS:CONFIRMED\r
DTSTART;TZID=Europe/Brussels:" . $starts . "\r
DTEND;TZID=Europe/Brussels:" . $ends . "\r
LOCATION:" . $event['address'] . "\r
END:VEVENT\r\n";
};
 
$output .= "END:VCALENDAR";
 
echo $output;
?>
