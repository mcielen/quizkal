<?php

require('../dbconfig');

$formid = $_POST["form_id"];

if (! $formid == 14803) {
  echo "invalid form id";
} else {
  $name = $_POST["element_1"];
  $mail = $_POST["element_2"];
  $stmt = $pdo->prepare("INSERT INTO users (name, mail) VALUES( :name, :mail)");
  $stmt->execute(['name' => $name, 'mail' => $mail]);
  $inserted = $stmt->rowCount();
  echo $inserted;
}

