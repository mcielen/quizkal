<?php
require('../dbconfig');
require '../PHPMailer/PHPMailerAutoload.php';

$stmt_users = $pdo->query("SELECT id, name, mail from users");
while ($row = $stmt_users->fetch()){
  $mail = new PHPMailer;
  $mail->isSMTP();
  $mail->Host = getenv("MAIL_HOST");  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = getenv("MAIL_USER");                 // SMTP username
  $mail->Password = getenv("MAIL_PASS");                           // SMTP password
  $mail->SMTPSecure = 'tls';
  $mail->setFrom(getenv("MAIL_USER"), getenv("MAIL_FROM"));
  $mail->addAddress($row['mail']);
  $mail->isHTML(true);

  $user = $row['id'];
  $name = $row['name'];
 
  $mail->Subject = "Quizoverzicht voor $name";

  $mail->Body .= "<h2>Quizoverzicht voor $name</h2>";
  $mail->Body .= "<br>";

  // query om events te krijgen waar gebruiker niet op gereageerd heeft.
  $stmt_nr = $pdo->query("select events.id as eventid, events.datetime, events.title, events.participants, events.address, reactions.id as reactionid, reactions.user_id, reactions.reaction from events LEFT JOIN reactions ON events.id = reactions.event_id AND reactions.user_id = $user WHERE reactions.event_id IS NULL");

  // query om events te krijgen waar gebruiker wel op gereageerd heeft.
  $stmt_r = $pdo->query("SELECT events.id as eventid, events.datetime, events.title, events.participants, events.address, reactions.id as reactionid, reactions.user_id, reactions.reaction from events left outer join reactions on (events.id = reactions.event_id) where reactions.user_id = $user and reactions.reaction = 1");


  $mail->Body .= "<h3>Quizzen waar je voor ingeschreven bent</h3>";
  while ($row = $stmt_r->fetch()){
    $eventid = $row['eventid'];
    $stmt_countpart = $pdo->query("select count(*) from reactions where reaction = 1 and event_id = $eventid");
    // query voor lijst deelnemers
    $stmt_partnames = $pdo->query("select name from events left join reactions on reactions.event_id = events.id join users on users.id = reactions.user_id where events.id = $eventid and reactions.reaction = 1");
    $count = $stmt_countpart->fetchColumn();
    $mail->Body .= $row['datetime'] . "    " . $row['title'] . "    " . $count . "/" . $row['participants'] . " deelnemers    " . $row['address'];
    $mail->Body .= "<br>Doen mee: ";
    while ($rowp = $stmt_partnames->fetch()){
      $mail->Body .= $rowp['name'] . " ";
    }
    $mail->Body .= "<br><br>";
  }

  $mail->Body .=  "<br><h3>Quizzen waar je nog niet op gereageerd hebt</h3>";
  $mail->Body .= "<table>";
  while ($row = $stmt_nr->fetch()){
    $eventid = $row['eventid'];
    $stmt_countpart = $pdo->query("select count(*) from reactions where reaction = 1 and id = $eventid");
    $count = $stmt_countpart->fetchColumn();
    $mail->Body .= "<tr>";
    $mail->Body .= "<td>";
    $mail->Body .= $row['datetime'] . "</td><td>" . $row['title'] . "</td><td>" . $count . "/" .$row['participants'] . " deelnemers</td><td>" . $row['address'];
    $mail->Body .= "</td><td>";
    $arr_yes = array('user_id' => $user, 'event_id' => $row['eventid'], 'reaction' => '1');
    $arr_no = array('user_id' => $user, 'event_id' => $row['eventid'], 'reaction' => '0');
    $mail->Body .= "<a style=\"font: bold 11px Arial; text-decoration: none; background-color: #009900; color: #ffffff; padding: 2px 6px 2px 6px; border-top: 1px solid #CCCCCC; border-right: 1px solid #333333; border-bottom: 1px solid #333333; border-left: 1px solid #CCCCCC;\" href=\"http://".getenv("MAIL_REACT_SERVER")."/form/react.php?" . urlencode(base64_encode(json_encode($arr_yes))) . "\">ik kom</a> ";
    $mail->Body .= "</td><td>";
    $mail->Body .= "<a style=\"font: bold 11px Arial; text-decoration: none; background-color: #990000; color: #ffffff; padding: 2px 6px 2px 6px; border-top: 1px solid #CCCCCC; border-right: 1px solid #333333; border-bottom: 1px solid #333333; border-left: 1px solid #CCCCCC;\" href=\"http://".getenv("MAIL_REACT_SERVER")."/form/react.php?" . urlencode(base64_encode(json_encode($arr_no))) . "\">ik kom niet</a>";
    $mail->Body .= "</td>";
    $mail->Body .= "</tr>";
  }
  $mail->Body .= "</table>";
if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo "Message to $name has been sent\n";
}

}
?>
