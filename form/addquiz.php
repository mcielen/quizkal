<?php

require('../dbconfig');

$formid = $_POST["form_id"];

if (! $formid == 14803) {
  echo "invalid form id";
} else {
  $datetime = $_POST['element_3_3'] . "-" . $_POST['element_3_2'] . "-" . $_POST['element_3_1'] . " " . $_POST['element_5_1'] . ":" . $_POST['element_5_2'] . ":00";
  $title = $_POST['element_2'];
  $participants = $_POST['element_6'];
  $address = $_POST['element_4'];
  $stmt = $pdo->prepare("INSERT INTO events (datetime, title, participants, address) VALUES( :datetime, :title, :participants, :address)");
  $stmt->execute(['datetime' => $datetime, 'title' => $title, 'participants' => $participants, 'address' => $address]);
  $inserted = $stmt->rowCount();
  echo $inserted;
}

